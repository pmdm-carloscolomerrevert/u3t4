package dam.android.carlos.u3t4event;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class eventData extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    private TextView tvEventName, tvDate = null, tvTime = null;
    private RadioGroup rgPriority;
    private Button btnAccept;
    private Button btnCancel;
    private Button btnDatePicker;
    private Button btnTimePicker;
    private String priority = "Normal";
    private Resources res;
    private String[] month;
    private EditText etPlace;
    private Calendar calendar;
    private int year, months, dayOfMonth, hour, minute;
    private String date = "", hourMinute = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_data);

        setUI();
        //get data from Intent that started this activity
        Bundle imputData = getIntent().getExtras();
        //set eventName from inputData
        tvEventName.setText(imputData.getString("EventName"));
        //Todo 1.2-2: Declaramos el array de string como global, y asignamos los valores que hay en strings.xml
        res = getResources();
        month = res.getStringArray(R.array.month_array);


    }

    private void setUI() {

        tvEventName = (TextView) findViewById(R.id.tvEventName);
        rgPriority = (RadioGroup) findViewById(R.id.rgPriority);
        rgPriority.check(R.id.rbNormal);
        tvDate = (TextView) findViewById(R.id.tvDate);
        tvTime = (TextView) findViewById(R.id.tvTime);

        btnAccept = (Button) findViewById(R.id.btnAccept);
        btnCancel = (Button) findViewById(R.id.btnCancel);
        etPlace = (EditText) findViewById(R.id.etPlace);
        btnDatePicker = (Button) findViewById(R.id.btnDatePicker);
        btnTimePicker = (Button) findViewById(R.id.btnTimePicker);
        //TODO 4.1: implementem el datePicker y el timePicker
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        months = calendar.get(Calendar.MONTH);
        dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        minute = calendar.get(Calendar.MINUTE);

        btnDatePicker.setOnClickListener(this);
        btnTimePicker.setOnClickListener(this);
        btnAccept.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        rgPriority.setOnCheckedChangeListener(this);


    }

    @Override
    public void onClick(View v) {
        Intent activityResult = new Intent();
        Bundle eventData = null;

        switch (v.getId()) {
            case R.id.btnAccept:
                //TODO 1.3-1: Añadimos los datos de lugaar y el formato de fecha
                eventData = new Bundle();
                eventData.putString("EventData", "Place: " + etPlace.getText() + "\n" + "Priority: " + priority + "\n" + "Date: "
                        + date + "\n" + "Hour: " + hourMinute);

                break;
            case R.id.btnCancel:

                eventData.putString("EventData", "");

                break;

            case R.id.btnDatePicker:
                //TODO 4.2: guardamos la fecha que hemos seleccionado en el calendario
                DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int day) {

                                calendar.set(Calendar.YEAR, year);
                                calendar.set(Calendar.MONTH, month);
                                calendar.set(Calendar.DAY_OF_MONTH, day);

                                String myFormat = "dd/MM/yyyy";
                                SimpleDateFormat format = new SimpleDateFormat(myFormat, Locale.US);

                                date = format.format(calendar.getTime());
                                //Asignamos la fecha seleccionada al textView para que sepa que fecha ha seleccionado
                                tvDate.setText(date);
                            }
                        }, year, months, dayOfMonth);


                datePickerDialog.show();

                break;

            case R.id.btnTimePicker:
                //TODO 4.3: guardamos la hora seleccionada en el reloj
                TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        hourMinute = hourOfDay + ":" + minute;
                        //Asignamos la hora seleccionada al textView para que sepa que hora ha seleccionado
                        tvTime.setText(hourMinute);
                    }
                }, hour, minute, true);

                timePickerDialog.show();


                break;

        }


        if (eventData != null) {

            activityResult.putExtras(eventData);
            setResult(RESULT_OK, activityResult);

            finish();
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int i) {
        //Cambiamos el nombre de las prioridades para que nos muestre el mensaje de prioridad en el idioma seleccionado.
        switch (i) {
            case R.id.rbLow:
                priority = getString(R.string.rbLow);
                break;
            case R.id.rbNormal:
                priority = getString(R.string.rbNormal);
                break;
            case R.id.rbHigh:
                priority = getString(R.string.rbHigh);
                break;
        }

    }

    //Creamos los metodos para guardar los datos necesarios

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        CharSequence tvTimeText = tvTime.getText();
        CharSequence tvDateText = tvDate.getText();
        CharSequence dateFormat = date;
        CharSequence hourFormat = hourMinute;


        outState.putString("tvTimeText", tvTimeText.toString());
        outState.putString("tvDateText", tvDateText.toString());
        outState.putString("dateFormat", dateFormat.toString());
        outState.putString("hourFormat", hourFormat.toString());


    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        CharSequence tvTimeText = savedInstanceState.getCharSequence("tvTimeText");
        CharSequence tvDateText = savedInstanceState.getCharSequence("tvDateText");
        CharSequence dateFormat = savedInstanceState.getCharSequence("dateFormat");
        CharSequence hourFormat = savedInstanceState.getCharSequence("hourFormat");

        tvTime.setText(tvTimeText);
        tvDate.setText(tvDateText);
        date = dateFormat.toString();
        hourMinute = hourFormat.toString();
    }
}
