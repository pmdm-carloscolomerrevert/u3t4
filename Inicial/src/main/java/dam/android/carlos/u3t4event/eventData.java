package dam.android.carlos.u3t4event;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

import org.w3c.dom.Text;

import java.util.Date;

public class eventData extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener{

    private TextView tvEventName;
    private RadioGroup rgPriority;
    private DatePicker dpDate;
    private TimePicker tpTime;
    private Button btnAccept;
    private Button btnCancel;
    private String priority="Normal";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_data);

        setUI();
        //get data from Intent that started this activity
        Bundle imputData = getIntent().getExtras();
        //set eventName from inputData
        tvEventName.setText(imputData.getString("EventName"));
    }

    private void setUI() {

        tvEventName = (TextView)findViewById(R.id.tvEventName);
        rgPriority = (RadioGroup)findViewById(R.id.rgPriority);
        rgPriority.check(R.id.rbNormal);
        dpDate = (DatePicker)findViewById(R.id.dpDate);
        tpTime = (TimePicker)findViewById(R.id.tpTime);
        tpTime.setIs24HourView(true);
        btnAccept = (Button)findViewById(R.id.btnAccept);
        btnCancel = (Button)findViewById(R.id.btnCancel);

        btnAccept.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        rgPriority.setOnCheckedChangeListener(this);

    }

    @Override
    public void onClick(View v) {
        Intent activityResult = new Intent();
        Bundle eventData = new Bundle();

        switch (v.getId()){
            case R.id.btnAccept:

                String[] month= {"January", "February", "March", "April", "May", "June", "July", "August", "September",
                        "October", "November", "December"};

                eventData.putString("EventData", "Priority:"+ priority + "\n"+ "Month: " + month[dpDate.getMonth()] +
                        "\n" + "Day: " + dpDate.getDayOfMonth() + "\n" + "Year: " + dpDate.getYear());

                break;
            case R.id.btnCancel:

                eventData.putString("EventData", "");

                break;
        }

        activityResult.putExtras(eventData);
        setResult(RESULT_OK, activityResult);

        finish();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int i) {

        switch (i){
            case R.id.rbLow:
                priority="Low";
                break;
            case R.id.rbNormal:
                priority="Normal";
                break;
            case R.id.rbHigh:
                priority="High";
                break;
        }

    }
}
