package dam.android.carlos.u3t4event;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private final int REQUEST = 0;

    private EditText etEventName;
    private TextView tvCurrentData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
    }

    private void setUI() {

        etEventName=(EditText)findViewById(R.id.etEventName);
        tvCurrentData=(TextView)findViewById(R.id.tvCurrentData);

    }

    public void editEventData(View v){

        Intent intent = new Intent(this, eventData.class);
        Bundle bundle = new Bundle();

        //set info data to bundle
        bundle.putString("EventName", etEventName.getText().toString());
        //add bundle to intent
        intent.putExtras(bundle);

        startActivityForResult(intent, REQUEST);

    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //TODO 1.1: Solo modificamos el texto de tvCurrentData si la información que llega desde el extra no está vacio
        //check if result comes from eventData and finised OK
        if(requestCode == REQUEST && resultCode == RESULT_OK && !data.getStringExtra("EventData").equals("")){
            tvCurrentData.setText(data.getStringExtra("EventData"));
        }
    }

    //TODO 1.3-3: Definimos los metodos necesarios para preservar el estado de tvCurrentData al cambio de configuración

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        CharSequence tvCurrent = tvCurrentData.getText();

        outState.putString("tvCurrentData",tvCurrent.toString());
        Log.i("Log", tvCurrentData.getText().toString());

    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        CharSequence tvCurrent = savedInstanceState.getCharSequence("tvCurrentData");;

        tvCurrentData.setText(tvCurrent);
    }
}
