package dam.android.carlos.u3t4event;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

import org.w3c.dom.Text;

import java.util.Date;

public class eventData extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener{

    private TextView tvEventName;
    private RadioGroup rgPriority;
    private DatePicker dpDate;
    private TimePicker tpTime;
    private Button btnAccept;
    private Button btnCancel;
    private String priority="Normal";
    private Resources res;
    private String[] month;
    private EditText etPlace;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_data);

        setUI();
        //get data from Intent that started this activity
        Bundle imputData = getIntent().getExtras();
        //set eventName from inputData
        tvEventName.setText(imputData.getString("EventName"));
        //Todo 1.2-2: Declaramos el array de string como global, y asignamos los valores que hay en strings.xml
        res = getResources();
        month = res.getStringArray(R.array.month_array);
    }

    private void setUI() {

        tvEventName = (TextView)findViewById(R.id.tvEventName);
        rgPriority = (RadioGroup)findViewById(R.id.rgPriority);
        rgPriority.check(R.id.rbNormal);
        dpDate = (DatePicker)findViewById(R.id.dpDate);
        tpTime = (TimePicker)findViewById(R.id.tpTime);
        tpTime.setIs24HourView(true);
        btnAccept = (Button)findViewById(R.id.btnAccept);
        btnCancel = (Button)findViewById(R.id.btnCancel);
        etPlace = (EditText)findViewById(R.id.etPlace);

        btnAccept.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        rgPriority.setOnCheckedChangeListener(this);

    }

    @Override
    public void onClick(View v) {
        Intent activityResult = new Intent();
        Bundle eventData = new Bundle();

        switch (v.getId()){
            case R.id.btnAccept:
                //TODO 1.3-1: Añadimos los datos de lugaar y el formato de fecha
                eventData.putString("EventData","Place: " + etPlace.getText() + "\n" + "Priority: "+ priority + "\n" + "Date: " +
                        dpDate.getDayOfMonth()+ " " + month[dpDate.getMonth()]  + " " + dpDate.getYear()
                + "\n" + "Hour: " + tpTime.getHour() + ":" + tpTime.getMinute());

                break;
            case R.id.btnCancel:

                eventData.putString("EventData", "");

                break;
        }

        activityResult.putExtras(eventData);
        setResult(RESULT_OK, activityResult);

        finish();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int i) {

        switch (i){
            case R.id.rbLow:
                priority="Low";
                break;
            case R.id.rbNormal:
                priority="Normal";
                break;
            case R.id.rbHigh:
                priority="High";
                break;
        }

    }
}
